# -*- coding: utf-8 -*-

import bs4 as bs
import nltk
import sqlite3 as sql
import collections
import os

LANG = "french"

# Classe representant la base de donnees et contenant les operations de base (CRUD)
class BaseDonnees:

    def __init__(self, bdd):
        # connexion à la BDD
        self.db_file = bdd
        self.conn = sql.connect(bdd, timeout=30000)
        self.cur = self.conn.cursor()

# Classe representant un document
class Document:

    def __init__(self, htmlDoc):
        # conserver le nom du document
        self.htmlDoc = htmlDoc
        # charger le document HTML brut
        self.htmlContent = bs.BeautifulSoup(open(htmlDoc, "r"), "html.parser")
        # stocker le body en supprimant les tags inutiles
        self.clean_useless_tags()
        try:
            # this might be a problem for web pages not containing a body (D7, D106)
            self.body = self.htmlContent.body.getText()
        except AttributeError:
            print "Error with " + htmlDoc
            self.body = ""

        # stemmer le body
        self.stem_body()


    # Supprime un tag du body
    def remove_tag(self, tag):
        for t in self.htmlContent.find_all(tag):
            t.decompose()

    # Supprime tous les tags inutiles du body
    def clean_useless_tags(self):
        tags = ["script", "noscript", "a", "img"]
        map(self.remove_tag, tags)


    # Stemme tous les mots du body
    def stem_body(self):
        # instanciation du stemmer
        stemmer = nltk.stem.snowball.SnowballStemmer(LANG, ignore_stopwords=True)
        # tokenizer le body
        tokenized_body = nltk.word_tokenize(self.body)
        # supprimer les mots inutiles (on pourra rajouter des mots comme "Allocine")
        filtered_words = [word for word in tokenized_body if word not in nltk.corpus.stopwords.words(LANG)]
        self.stemmed_body = map(stemmer.stem, filtered_words)

    def indexer(self):
        if (self.stemmed_body is not None):
            self.index = list(collections.Counter(self.stemmed_body).items())


    def stocker(self, database):
        # on fait cela pour supprimer toutes les entrees du document dans l'index inverse
        database.cur.execute('delete from documents where name = ?;', (self.htmlDoc,))
        database.cur.execute('insert into documents (name) values (?);', (self.htmlDoc,))

        for indexed_word in self.index:
            # on insere ou ignore le mot courant dans la liste
            database.cur.execute('insert or ignore into words (word) values (?);', (indexed_word[0],))
            # l'index inverse a ete vide de toutes ses entrees concernant le document courant donc on peut insert sans se soucier de savoir s'il y a des residus
            database.cur.execute('insert into inverted_index (word_id, document_id, frequency) values ((select id from words where word = ?), (select id from documents where name = ?), ?);', (indexed_word[0], self.htmlDoc, indexed_word[1],))


        database.conn.commit()



# MAIN
bdd = BaseDonnees("../resources/index.db")


for file in os.listdir("../resources/corpus-utf8"):
    if file.endswith(".html"):
        print(file)
        doc = Document("../resources/corpus-utf8/" + file)
        doc.indexer()
        doc.stocker(bdd)

bdd.conn.close()