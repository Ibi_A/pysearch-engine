import kw_by_doc_matrix as kd
import qr_by_kw_matrix as qk
import numpy as np

def match_doc_similarity_by_request(similarity_matrix, queries_list, documents_list):
    t_sim_matrix = similarity_matrix.T.tolist()
    tuple_matrix = []

    i = 0
    for query in queries_list:
        j = 0
        curr_query_vector = []
        for document in documents_list:
            curr_query_vector.append((queries_list[i], documents_list[j], t_sim_matrix[i][j]))
            j = j+1
        tuple_matrix.append(curr_query_vector)
        i = i+1

    # cannot return a numpy matrix because each coefficient is a tuple (>2D)
    return tuple_matrix

def sort_doc_similarity_by_request_matrix(matrix):
    sorted_matrix = []

    for request in matrix:
        current_sorted_request = sorted(request, key=lambda tuple: tuple[2], reverse=True)
        sorted_matrix.append(current_sorted_request)

    return sorted_matrix

def display_matrix(matrix):
    s = [[str(e) for e in row] for row in matrix]
    lens = [max(map(len, col)) for col in zip(*s)]
    fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
    table = [fmt.format(*row) for row in s]
    print '\n'.join(table)

qr_by_kw = qk.generate_matrix_from_file("../resources/queries")
kw_by_doc = kd.generate_matrix_from_keywords(qr_by_kw["keywords"])




similarity_matrix = kw_by_doc["matrix"].dot(qr_by_kw["matrix"])

print str(qr_by_kw["matrix"].shape)
print(similarity_matrix)

print "similarity matrix shape: " + str(similarity_matrix.shape)
print qr_by_kw["matrix"]
print qr_by_kw["keywords"]
print qr_by_kw["stemmed_queries"]
print kw_by_doc["matrix"]


doc_similarity_by_request =  match_doc_similarity_by_request(similarity_matrix, qr_by_kw["stemmed_queries"], kw_by_doc["documents"])
display_matrix(sort_doc_similarity_by_request_matrix(doc_similarity_by_request))