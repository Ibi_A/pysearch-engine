# -*- coding: utf-8 -*-


import sqlite3 as sql
import numpy as np
import math


class DatabaseConnection:

    def __init__(self, location):
        self.db_file = location
        self.conn = sql.connect(location, timeout=30000)
        self.cur = self.conn.cursor()


def fetch_data_from_db(db_location, keywords):
    db = DatabaseConnection(db_location)
    inverted_index = []
    documents = []

    for keyword in keywords:
        inverted_index.append(db.cur.execute('select words.word, documents.name, inverted_index.frequency from words, documents, inverted_index where words.id = inverted_index.word_id and documents.id = inverted_index.document_id and words.word = ?;', (keyword,)).fetchall())

    inverted_index = [item for sublist in inverted_index for item in sublist]
    inverted_index = list(set(inverted_index))

    for row in inverted_index:
        documents.append(row[1])

    documents = list(set(documents))

    return {"inverted_index": inverted_index, "documents": documents}


def generate_idf_vector(keywords, documents, inverted_index):
    nb_docs = len(documents)
    ii_no_freq = []
    idf_vector = []

    for tuple in inverted_index:
        ii_no_freq.append((tuple[0], tuple[1]))


    for keyword in keywords:
        print "current keyword: " + keyword
        curr_keyword_occur_in_docs = 0

        for document in documents:
            if (keyword, document) in ii_no_freq:
                print "     found " + keyword + " in document " + document
                curr_keyword_occur_in_docs = curr_keyword_occur_in_docs + 1


        print str(curr_keyword_occur_in_docs) + " documents containing " + keyword

        # Probleme majeur : l'indexeur n'indexe pas les nombres (2012) et souci d'accent (recompense != r&eacutecompense) ce qui fait que curr_keyword_occur_in_docs vaut 0 ce qui fait que idf plante !!!
        # idf = math.log(nb_docs / curr_keyword_occur_in_docs)
        idf = 1
        idf_vector.append(idf)

    print idf_vector

    return idf_vector




def generate_matrix(keywords, documents, inverted_index):

    ii_no_freq = []
    matrix = []
    idf_vector = generate_idf_vector(keywords, documents, inverted_index)

    for tuple in inverted_index:
        ii_no_freq.append((tuple[0], tuple[1]))

    for document in documents:
        curr_document_row = []

        keyword_id = 0
        for keyword in keywords:
            if (keyword, document) in ii_no_freq:
                i = ii_no_freq.index((keyword,document))
                freq = inverted_index[i][2]
                # here is TF/IDF
                tf = freq
                curr_document_row.append(tf * idf_vector[keyword_id])
            else:
                curr_document_row.append(0)
            keyword_id = keyword_id + 1

        matrix.append(curr_document_row)


    matrix = np.matrix(matrix)

    print("===END===")
    print
    print("# documents (lines)")
    print(np.matrix(documents).T)
    print
    print("# keywords (columns)")
    print(keywords)
    print
    print("# generated matrix")
    print(matrix)
    print

    return matrix


def generate_matrix_from_keywords(keywords):
    data = fetch_data_from_db("../resources/index.db", keywords)
    matrix = generate_matrix(keywords, data["documents"], data["inverted_index"])

    return {"matrix": matrix, "documents": data["documents"]}