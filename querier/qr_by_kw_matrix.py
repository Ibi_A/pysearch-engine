# -*- coding: utf-8 -*-
import numpy as np
import sys
import nltk

# Fetches each query in filename into a list where a single line is a query 
def get_raw_queries(filename):
	with open(filename) as f:
	    queries_list = f.read().splitlines()

	return queries_list

# Stems a list of query
def stem_queries(queries_list):
	stopwords = nltk.corpus.stopwords.words("french")
	stemmer = nltk.stem.snowball.SnowballStemmer("french")
	stemmed_queries_list = []

	for query in queries_list:
		tokenized_body = nltk.word_tokenize(query)
		cleaned_query = [word.lower() for word in tokenized_body if (word not in stopwords and len(word) > 1)]
		stemmed_queries_list.append(map(stemmer.stem, cleaned_query))
	
	return stemmed_queries_list

# Puts all queries into a single list where each keyword appears only once
def flatten_keywords(keywords_list):
	flattened_keywords_list = [item for sublist in keywords_list for item in sublist]
	unique_list = list(set(flattened_keywords_list))
	return unique_list

# Generates the matrix matching the presence of a keyword in a query 
def generate_matrix(stemmed_queries, flattened_queries_list):
	matrix = []
	# iterate on keywords
	for keyword in flattened_queries_list:
		print("# current keyword: " + keyword)
		current_keyword_vector = []
	
		for query in stemmed_queries:
			print(query)
			if keyword in query:
				print("    -> " + keyword + " found!")
				current_keyword_vector.append(1)
			else:
				print("    -> " + keyword + " not found...")
				current_keyword_vector.append(0)
		# add the keyword's vector to the matrix
		matrix.append(current_keyword_vector)
		print

	# transform the list into a np.matrix
	matrix = np.matrix(matrix)

	print("===END===")
	print
	print("# keywords (lines)")
	print(np.matrix(flattened_queries_list).T)
	print
	print("# queries (columns)")
	print(stemmed_queries)
	print
	print("# generated matrix")
	print(matrix)
	print

	return matrix

def generate_matrix_from_file(filename):
	queries_list = get_raw_queries(filename)
	stemmed_queries = stem_queries(queries_list)
	flattened_keywords_list = flatten_keywords(stemmed_queries)
	matrix = generate_matrix(stemmed_queries, flattened_keywords_list)

	return {"matrix": matrix, "keywords": flattened_keywords_list, "stemmed_queries": stemmed_queries, "queries_list": queries_list}
